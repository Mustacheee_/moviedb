package com.jbink.mustacheee.moviedb;

import android.content.Context;
import android.net.Uri;
import android.os.AsyncTask;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Vector;

/**
 * Created by benja on 2/23/2016.
 */
public class FetchMovieTask extends AsyncTask<Void, Void, String[]> {
    @Override
    protected String[] doInBackground(Void... params) {
        Vector<String> title = new Vector<String>();

        Uri.Builder builder = new Uri.Builder();
        builder.scheme("http")
                .authority("www.omdbapi.com")
                .appendQueryParameter("t","inception")
                .appendQueryParameter("y", "2016")
                .appendQueryParameter("plot", "short")
                .appendQueryParameter("r", "json");

        HttpURLConnection urlConnection = null;
        BufferedReader bufferedReader = null;
        try {
            URL url = new URL(builder.build().toString());
            urlConnection = (HttpURLConnection) url.openConnection();
            urlConnection.setRequestMethod("GET");
            urlConnection.connect();

            InputStream in = new BufferedInputStream(urlConnection.getInputStream());
            StringBuffer buffer = new StringBuffer();

            if(in == null)
                return null;

            bufferedReader = new BufferedReader(new InputStreamReader(in));

            String line;
            while((line = bufferedReader.readLine()) != null)
                buffer.append(line + "\n");

            if(buffer.length() == 0)
                return null;

            String jsonStr = buffer.toString();

            JSONObject movie = new JSONObject(jsonStr);

            title.add(movie.getString("Title"));
            title.add(movie.getString("Poster"));




        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        catch(IOException e) {
            Log.e("SWOOP", e.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        } finally {
            if(urlConnection != null)
                urlConnection.disconnect();

            if(bufferedReader != null){
                try {
                    bufferedReader.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return convertVector(title);
    }

    private String[] convertVector(Vector<String> v){
        String[] results = new String[v.size()];
        int i = 0;
        for (String s : v){
            results[i] = s;
            i++;
        }
        return results;
    }
}
